# FabZero @ FabLab ULB

## Introduction

Dans le cadre du cours de "Design" et "How To Make (almost) any Experiments Using Digital fabrication" à l'ULB et des formations au FabLab ULB, nous façonnons un support de formation commun et réalisé de manière collaborative avec différents membres de la communauté du FabLab ULB.

Cette documentation "noyau" et modulaire s'intitule FabZero @ FabLab ULB (en référence au programme [FabZero officiel](https://github.com/Academany/fabzero) organisé par la Fab Foundation) et le principe est quelle puisse se décliner suivant les besoins des différents cours à l'université et au FabLab ULB e.g. orienté vers le design pour les architectes ou orienté vers les expériences scientifiques pour les sciences ou orienté tout public pour les formations au FabLab ULB, ...

Chaque module de formation contient une formation en présentiel, un support de formation et un exercice pratique à réaliser.

Formation en présentiel : différents intervenants experts dans une technique viendront apprendre aux étudiants à utiliser l'un ou l'autre outil. Ces intervenants réaliseront un support de formation sur la plateforme Gitlab du cours.

Chaque étudiant devra réaliser un exercice pratique démontrant ses capacités à utiliser l'outil. L'étudiant rendra donc compte de son travail par une réalisation technique et une documentation de son processus de conception (avec les essais erreurs).

## Table of Contents

* Basics - Project Management
  * [Documentation - Denis Terwagne](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/Documentation.md)
  * GIT & Gitlab

* Computer-Aided Design
  * 3D Design
    * [Fusion 360 - Thibaut Baes](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/Fusion360.md)
    * [OpenSCAD - Nicolas De Coster](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/OpenSCAD.md)

* Computer-controlled cutting
  * [Laser Cutter - Axel Cornu](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)
  * Vinyl Cutter -

* 3D Scanning and printing
  * [3D Printing - Gwen Best & Hélène Bardijn](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/3DPrinters.md)
  * 3D Scanning -

* Computer Controlled Machining
  * CNC -
  * Shaper -

* Electronics
  * Electronics production
    * [Make your own Arduino - Nicolas De Coster](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md)

## Credits

#### FabZero @ FabLab ULB

Direction : Victor Lévy & Denis Terwagne  
Documentation editor : Denis Terwagne

#### Other credits

These educational modules are greatly inspired from the popular MIT class How To Make (almost) Anything instructed by Prof. Neil Gershenfeld, the Fab Academy global training and the FabZero official program.
