# Tasks automation

## [Gitlab API](https://docs.gitlab.com/ee/api/)

- [Get/Set group and project members](https://docs.gitlab.com/ee/api/members.html)

## Other utilities and tools

- [jq](https://www.howtogeek.com/529219/how-to-parse-json-files-on-the-linux-command-line-with-jq/) : to use json in bash

## Procédure

Sur base d'un canevas, nous générons une série de projets gitlab pour les étudiants des différents cours. Chaque étudiant a son projet.
Généralement, le canevas est l'architecture d'un projet de site web qui pourra être généré via mkdocs.
Nous générons une liste json avec le nom du projet (firstname.surname) et le "gitlab id" de chaque étudiant.
Chaque étudiant est "maintainer" de son projet.
Nous générons également un projet "class" pour échanger des infos ou de la doc où tous les étudiants sont "maintainer".
Nous ajoutons un "schedule" pour ne lancer la procédure de génération du site web automatique qu'à deux moments dans la semaine. Ceci afin d'éviter de saturer les serveurs gitlab (le .gitlab-ci.yml dans les projets des étudiants est adapté en conséquence).

génération d'un projet pour chaque étudiant :
  clone_baseProj_for_students.sh (changer le "REMOTE_TARGET" et mettre à jour le dossier firsname.surname)
	usage: ./clone_baseProj_for_students.sh List.json

donner les permissions aux étudiants par rapport à leur projet :
  add_student_projMember.py (changer le "group id" et le nom de la liste et générer un token)
	usage: ./add_student_projMember.py | sh -x

ajout d'un cron pour lancer le CI/CD à des moments précis :
  add_schedule.py  (changer le "group id" et le nom de la liste et générer un token)
 	usage: ./add_schedule.py | sh -x

