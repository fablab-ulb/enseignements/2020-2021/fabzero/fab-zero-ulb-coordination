#!/usr/bin/env python3


# Date    : 2020-10-08
# Author  : Nicolas H.-P. De Coster <decostern@gmail.com>
# Version : 0.0 draft
# Description : This script is a quick and dirty solution to automate projects rights for students
# Usage   : the script will output the bash command necessary to take actions.
#           So, to check the output : simply run the script : ./add_student_projMember.py
#           Once everything seems ok pipe it with shell (-x to get verbose output) : ./add_student_projMember.py | sh -x

# note : change visibility of project coordination : curl --request PUT --header "PRIVATE-TOKEN: oVsGi2DDCHsuFqL5KvbA" --data "visibility=private" "https://gitlab.com/api/v4/projects/21650543"

# doc about API members : https://docs.gitlab.com/ee/api/members.html

from urllib.request import urlopen
import json
import subprocess, shlex
import csv

group         = 63508417  #group id
coord_proj    = 43278905  #coordination project
list_file     = "230213_2022-2023_fabzero-exp_List.json"
access_level  = 40 #maintainer https://docs.gitlab.com/ee/user/permissions.html & https://docs.gitlab.com/ee/api/members.html
coord_access  = 40 #maintainer   https://docs.gitlab.com/ee/user/permissions.html
token_file    = "token.nogit"
visibility    = "public"
pages_access_level = "public" #One of disabled, private, enabled, or public. 

token=""
#Read the token from token.nogit
try:
    f = open(token_file, "r")
    token = f.read().strip()
except IOError:
    print("You must have a file named '" + token_file + "' in the repository with your private token (single line, no extra char.!!!) to run this script.")
    exit()
finally:
    f.close()

allProjects = urlopen("https://gitlab.com/api/v4/groups/" + str(group) + "?private_token=" + token + "&per_page=1000")
allProjectsDict = json.loads(allProjects.read().decode())

allStudents = []
with open(list_file) as json_file:
    allStudents = json.load(json_file)

for thisProject in allProjectsDict['projects']:
    try:
        pURL  = thisProject['ssh_url_to_repo']
        pID   = thisProject['id']
        for student in allStudents:
            if student['project'] in pURL and student['student_id'] != 0:
                sID    = student['student_id']
                pName  = student['project']
                addCommand = 'curl --request POST --header "PRIVATE-TOKEN: '+ token + '" --data "user_id=' + str(sID) + '&access_level=' + str(access_level) + '" "https://gitlab.com/api/v4/projects/' + str(pID) + '/members"'
                changeCommand = 'curl --request PUT --header "PRIVATE-TOKEN: '+ token +'" --data "visibility=' + visibility + '&pages_access_level=' + pages_access_level + '" "https://gitlab.com/api/v4/projects/' + str(pID) + '"'
                print("echo \"Project: " + pName + '"')
                print('echo "S_ID: ' + str(sID) + '"')
                print(addCommand)
                print(changeCommand)
                print('echo ""')
                #editCommand = 'curl --request PUT --header "PRIVATE-TOKEN: '+ token + '" "https://gitlab.com/api/v4/projects/' + str(pID) + '/members/' + str(sID) + '?access_level=' + str(access_level) + '"'
                #print(editCommand)
                # failed test to make python execute the code directly (but better to do it manually?)
                #resultCode = subprocess.Popen(command)
    except Exception as e:
        print("Error on %s: %s" % (pURL, e.strerror))

# add all students to coordination project
for student in allStudents:
    sID = student['student_id']
    pName  = student['project']
    print("echo \"Project: " + pName + '"')
    print('echo "S_ID: ' + str(sID) + '"')
    if sID != 0:
        addToCoord = 'curl --request POST --header "PRIVATE-TOKEN: '+ token + '" --data "user_id=' + str(sID) + '&access_level=' + str(coord_access) + '" "https://gitlab.com/api/v4/projects/' + str(coord_proj) + '/members"'
        print(addToCoord)
