const JSONToHTMLList = (json) => {
  var html='';
  //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
  switch(typeof json){
    case "object" :
      if(Array.isArray(json)){
        html+='<ul>';
        json.forEach(value => {
          html+='<li>'+JSONToHTMLList(value)+'</li>';
        });
        html+='</ul>';
      }
      else{
        html+='<dl>';
        Object.entries(json).forEach(entry => {
  //         console.log(entry);
          const [key, value] = entry;
          html+='<dt>'+key+'</dt>';
          if(value){
            html+='<dd>'+JSONToHTMLList(value)+'</dd>';
          }
          else{
            html+='<dd></dd>';
          }
        });
        html+='</dl>';
      }
      break;
    case "string" :
      //https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
      var expressionURL = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gi;
      var regexURL = new RegExp(expressionURL);
      if(json.match(regexURL)){
        html+='<a href="'+json+'" target=_blank>'+json+'</a>';
      }
      else{
        html+=json;
      }
      break;
    case "number" :
    case "bigint" :
      html+=json.toString();
      break;
    case "boolean" :
      html+= '<em>'+ json ? 'true':'false' + '</em>';
      break;
    case "undefined" :
      html+= '<em>undefined</em>';
      break;
    case "function" :
      html+= '<em>function()...</em>';
      break;
    case "null" :
      html+= '<em>null</em>';
      break;
      //     default :
//       throw 'JSONToHTMLList expect its argument to be array or object';
  }
//   console.log(html);
  return(html);
}
