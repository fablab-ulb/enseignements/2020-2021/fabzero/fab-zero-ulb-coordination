class gitLab {
  constructor(token){
    if(token == "")
      throw 'No token provided.';
    this.token    = token;
    this.baseURL  = "https://gitlab.com/api/v4/";
  }
  
  static get accessLvl(){
    return  [
              {val: 0, text:"None"},
              {val: 5, text:"Minimal"},
              {val:10, text:"Guest"},
              {val:20, text:"Reporter"},
              {val:30, text:"Developer"},
              {val:40, text:"Maintainer"},
              {val:50, text:"Owner (groups only)"}
            ];
  }
  
  token(){
    return this.token;
  }

  getJSONasync(url, data={}){
    //add private token to data
    data = Object.assign(
      {},
      data,
      {private_token : this.token}
    );
    var value= $.ajax({ 
      url: url, 
      data : data,
      async: false
    }).responseText;
    return JSON.parse(value);
  }
  
  putAsync(url, data){
    //add private token to data
    data = Object.assign(
      {},
      data,
      {private_token : this.token}
    );
    var value= $.ajax({
      url: url, 
      method: "PUT",
      data : data,
      async: false
    }).responseText;
    return JSON.parse(value);
  }
  
  postAsync(url, data){
    //add private token to data
    data = Object.assign(
      {},
      data,
      {private_token : this.token}
    );
    console.log(url);
    console.log(data);
    var value= $.ajax({
      url: url, 
      method: "POST",
      data : data,
      async: false
    }).responseText;
    return JSON.parse(value);
  }
  
  groupURL(groupID){
    return this.baseURL + "groups/" + groupID;
  }
  
  getGroup(groupID){
    return this.getJSONasync(this.groupURL(groupID));
  }
  
  getProjects(groupID){
    return this.getGroup(groupID)['projects'];
  }

  subgroupsURL(groupID){
    return this.groupURL(groupID) + "/subgroups";
  }
  
  getSubgroups(groupID){
    return this.getJSONasync(this.subgroupsURL(groupID));
  }
  
  getSubgroupWithName(groupID, name){
    let subgroups = this.getSubgroups(groupID);
    let query = 'SELECT * FROM ? WHERE name="'+name+'"';
    let result = alasql(query, [subgroups]);
    if(result.length == 0)
      throw 'No subgroup found with name ' + name +
            ' for group ID ' + groupID + '.';
    return result[0];
  }
  
  projectURL(projectID){
    return this.baseURL + "projects/" + projectID;
  }
  
  projectMembersURL(projectID){
    console.log(this.projectURL(projectID) + "/members")
    return this.projectURL(projectID) + "/members";
  }
  
  getMembers(projectID){
    return this.getJSONasync(this.projectMembersURL(projectID));
  }
  
  
  setMember(projectID, memberID, lvl){
    //get members list
    var members = this.getMembers(projectID);
    //check if memberID already in project
    var isMemberInProject = alasql(
      'SELECT id FROM ? WHERE id='+memberID,
      [members]
    ).length ? true:false;
    //if yes, update
    if(isMemberInProject){
      this.putAsync(
        this.projectMembersURL(projectID) + "/" + memberID,
        {"access_level" : lvl}
      );
    }
    //if not, add
    else{
      console.log('adding user ' + memberID);
      this.postAsync(
        this.projectMembersURL(projectID),
        {"user_id":memberID, "access_level":lvl}
      );
    }
  }
  
}

class gitLabClassGroup {
  constructor(token, groupID){
    
    //create GitLab connection
    this.conn     = new gitLab(token);
    
    //store the group ID
    this.groupID  = groupID;
    console.log("Group ID:")
    console.log(this.groupID);

    //get the group JSON
    this.group    = this.conn.getGroup(this.groupID);
    console.log("Group JSON :")
    console.log(this.group);

    //get the subgroup with "students" name
    this.students = this.conn.getSubgroupWithName(groupID, 'students');
    //repopulate the group properly from its ID
    this.students = this.conn.getGroup(this.students['id']);
    //save connection and projects context
    var conn = this.conn;
    var studentsProjs = this.students.projects;
    //populate students projects members
    studentsProjs.forEach(function(part, index) {
      this[index].members = alasql(
        'SELECT id, name, web_url, access_level FROM ?',
        [ conn.getJSONasync(this[index]._links.members) ]
      );
//           this[index].schedules = conn.getJSONasync(this[index]._links.self + '/pipeline_schedules');
      this[index].schedules = alasql(
        'SELECT id, description, cron FROM ?',
        [ conn.getJSONasync(this[index]._links.self + '/pipeline_schedules') ]
      );
    }, studentsProjs);
    console.log("Students group :")
    console.log(this.students);

    //find the project in group with name "Class Website"
    let queryClass = 'SELECT * FROM ? WHERE name="Class Website"';
    this.class    = alasql(queryClass, [this.group['projects']])[0];
    console.log("Class project :");
    console.log(this.class);
  }
  
  shortDesc(){
    return {
      group : alasql(
                'SELECT description, id, name, visibility, web_url FROM ?',
                [[this.group]]
              )[0],
      classProj : alasql(
                'SELECT description, id, name, readme_url, ssh_url_to_repo, visibility, web_url FROM ?',
                [[this.class]]
              )[0],
      studentsProjs : alasql(
                'SELECT description, id, members, name, readme_url, schedules, ssh_url_to_repo, visibility, web_url FROM ?',
                [this.students['projects']]
              )
    }
  }
}
