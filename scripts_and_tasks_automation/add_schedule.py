#!/usr/bin/env python3


# Date    : 2020-10-21
# Author  : Nicolas H.-P. De Coster <decostern@gmail.com>
# Version : 0.0 draft
# Description : This script is a quick and dirty solution to automate projects rights for students
# Usage   : the script will output the bash command necessary to take actions.
#           So, to check the output : simply run the script : ./add_student_projMember.py
#           Once everything seems ok pipe it with shell (-x to get verbose output) : ./add_schedule.py | sh -x

# note : change visibility of project coordination : curl --request PUT --header "PRIVATE-TOKEN: oVsGi2DDCHsuFqL5KvbA" --data "visibility=private" "https://gitlab.com/api/v4/projects/21650543"

# create schedule : curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --form description="Build packages" --form ref="main" --form cron="0 1 * * 5" --form cron_timezone="UTC" --form active="true" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules"

# cron_name     = "Cron night wed-thu"
# cron_val      = "30 4 * * 4" # = thursday @ 04:30 a.m.
# cron_name     = "Cron night mon-tue"
# cron_val      = "30 4 * * 2" # = tuesday @ 04:30 a.m.


# doc about API members : https://docs.gitlab.com/ee/api/members.html
# doc about API schedules : https://docs.gitlab.com/ee/api/pipeline_schedules.html

from urllib.request import urlopen
import json
import subprocess, shlex
import csv

group         = 63508417  #group id
coord_proj    = 43278905 #coordination project
list_file     = "230213_2022-2023_fabzero-exp_List.json"
token_file    = "token.nogit"
target_branch = "main"

cron_name     = "Cron night mon-tue"
cron_val      = "30 4 * * 2" # = tuesday @ 04:30 a.m.
# ┌───────────── minute (0 - 59)
# │ ┌───────────── hour (0 - 23)
# │ │ ┌───────────── day of the month (1 - 31)
# │ │ │ ┌───────────── month (1 - 12)
# │ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday;
# │ │ │ │ │                                   7 is also Sunday on some systems)
# │ │ │ │ │
# │ │ │ │ │
# * * * * * <command to execute>


token=""
#Read the token from token.nogit
try:
    f = open(token_file, "r")
    token = f.read().strip()
except IOError:
    print("You must have a file named '" + token_file + "' in the repository with your private token (single line, no extra char.!!!) to run this script.")
    exit()
finally:
    f.close()

allProjects = urlopen("https://gitlab.com/api/v4/groups/" + str(group) + "?private_token=" + token + "&per_page=1000")
allProjectsDict = json.loads(allProjects.read().decode())

allStudents = []
with open(list_file) as json_file:
    allStudents = json.load(json_file)

for thisProject in allProjectsDict['projects']:
    try:
        pURL  = thisProject['ssh_url_to_repo']
        pID   = thisProject['id']
        for student in allStudents:
            if student['project'] in pURL and student['student_id'] != 0:
                sID    = student['student_id']
                pName  = student['project']
                schedCommand = 'curl --request POST --header "PRIVATE-TOKEN: '+ token + '" --form description="' + cron_name + '" --form ref="' + target_branch + '" --form cron="' + cron_val + '" --form cron_timezone="UTC" --form active="true" "https://gitlab.com/api/v4/projects/' + str(pID) + '/pipeline_schedules"'
                print("echo \"Project: " + pName + '"')
                print('echo "S_ID: ' + str(sID) + '"')
                #print(addCommand)
                print(schedCommand)
                print('echo ""')
    except Exception as e:
        print("Error on %s: %s" % (pURL, e.strerror))
