# Date    : 2020-10-14
# Author  : Denis Terwagne <D.Terwagne@gmail.com>
# Version : 0.0 draft
# Description : This script is a quick and dirty solution to create automatically a list of all the sudents website.
# Usage   : $ python3 gen_websites_list.py
# note :
# doc :

import json

list_file = "230213_Students-list-formatted.json"
# list_file = "230213_2022-2023_fabzero-exp_List.json"

allStudents = []
with open(list_file) as json_file:
    
# read a json file
    # file_contents = json_file.read()
    # print(file_contents)

# read and parse json file
    parsed_json = json.load(json_file)
    print(parsed_json)

outF = open("student-websites-list.md", "w")
outF.write('# List of FabZero-Design student websites')
outF.write("\n \n")

for student in parsed_json:
    outF.write('[')
    outF.write(student['Prenom'])
    outF.write(' ')
    outF.write(student['NOM'])
    # gitlab repo
    # outF.write("](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/")
    # gitlab website
    outF.write("](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/")
    outF.write(student['project'])
    outF.write(')  ')
    outF.write("\n")
outF.close()

# print(allStudents[2]['project'])

