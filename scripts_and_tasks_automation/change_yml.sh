#!/usr/bin/env bash

# Date   : 2020-10-20
# Author : Nicolas H.-P. De Coster (Vigon)
# Description : This script will copy firstname.surname .gitlab-ci.yml file and copy it to every student page
# Usage  : "./change_yml list.json"
#          with list.json containing students project's name/ids
# requires : sudo apt-get install jq => required to do queries on the json file in bash


REMOTE_TARGET="git@gitlab.com:fablab-ulb/enseignements/2020-2021/fabzero-design/"
REMOTE_REF="firstname.surname"
FILE_TO_COPY=".gitlab-ci.yml"

git clone "${REMOTE_TARGET}${REMOTE_REF}.git"

# sed to remove trailing and leading " characters in json strings
for project in $(cat $1 | jq .[].project | sed -e 's/^"//' -e 's/"$//') ; do

  echo ${project}
  git clone "${REMOTE_TARGET}${project}.git"
  cp "${REMOTE_REF}/${FILE_TO_COPY}" "${project}/"
  cd ${project}
  git add ${FILE_TO_COPY}
  git commit -m "modified file ${FILE_TO_COPY}, replaced by a copy from reference project ${REMOTE_REF}"
  git push
  cd ..
  rm -rf ${project}
  
done

rm -rf ${REMOTE_REF}

# 
# while read student  ; do
#     cp -r firstname.surname ${student}
#     cd ${student}
#     git remote set-url origin git@gitlab.com:fablab-ulb/enseignements/2020-2021/fabzero-design/${student}.git
#     git push
#     cd ..
#     rm -rf ${student}
# done < $1
