#!/usr/bin/env bash

# Date   : 2020-10-02
# Author : Nicolas H.-P. De Coster (Vigon)
# Description : This script creates GitLab repositories based on a template for every line of a list file provided by user.
# Usage  : "./clone_baseProj_for_students.sh list.json"
#          with list.json containing a list of objects [{},{},...] containing "project" keys providing firstname.surname
# version note : 
# - Update for 2022-2023 ULB FabZero-Exp course (Denis Terwagne)

REMOTE_TARGET="git@gitlab.com:fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/"
REMOTE_REF="template"

git clone "${REMOTE_TARGET}${REMOTE_REF}.git"
ROOT_FOLDER=$(pwd)

if [ -d "$REMOTE_REF" ]; then
  for project in $(cat "$1" | jq .[].project | sed -e 's/^"//' -e 's/"$//') ; do
    cp -r "${REMOTE_REF}" "${project}"
    cd "${project}" || exit 1
    git remote set-url origin "${REMOTE_TARGET}${project}.git"
    git push
    cd "$ROOT_FOLDER" || exit 1
    rm -rf "${project}"
  done
fi
